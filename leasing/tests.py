#coding: utf-8
from django.test import TestCase, Client
from .models import Car, Company, PriceList
import pandas as pd
from datetime import datetime
import xlrd
from rest_framework.test import APITestCase
import json




# Create your tests here.
class ApiTest(APITestCase):
    def setUp(self):
        self.company1 = Company.objects.create(city='חיפה', name='אלדן', phone_num='098765677', is_active=True)
        # self.company1.save()
        self.company2 = Company.objects.create(city='יהוד', name='אביס', phone_num='888888888', is_active=True)
        # self.company2.save()
        self.company3 = Company.objects.create(city='ירושליים', name='אלבר', phone_num='777777777', is_active=False)
        # self.company3.save()
        self.car1 = Car.objects.create(name="פורד", timestamp = datetime.now())
        # self.car1.save()
        self.car2 = Car.objects.create(name="סובארו", timestamp = datetime.now())
        # self.car2.save()
        self.car3 = Car.objects.create(name="הונדה",  timestamp = datetime.now())
        # self.car3.save()
        self.price1 = PriceList.objects.create(company=self.company1, car=self.car1, price_at_company=50)
        # self.price1.save()
        self.price2 = PriceList.objects.create(company=self.company2, car=self.car2, price_at_company=80)
        # self.price2.save()
        self.price3 = PriceList.objects.create(company=self.company3, car=self.car3, price_at_company=100)
        # self.price3.save()

    def test_all_cars_status_code(self):
        c = Client()
        response = c.get('/leasing/cars/')
        print response
        return self.assertEquals(response.status_code, 200)

    def test_all_cars_count(self):
        c = Client()
        response = c.get('/leasing/cars/')
        all_cars_json_dict = json.loads(response.content)
        return self.assertEquals(all_cars_json_dict['count'], 3)


    def test_all_companies_status_code(self):
        c = Client()
        response = c.get('/leasing/company/')
        print response
        return self.assertEquals(response.status_code, 200)

    def test_all_companies_count(self):
        c = Client()
        response = c.get('/leasing/cars/')
        all_companies_json_dict = json.loads(response.content)
        return self.assertEquals(all_companies_json_dict['count'], 3)




class ModelsTestCase(APITestCase):
    def setUp(self):
        self.company1 = Company.objects.create(city='חיפה', name='אלדן', phone_num='098765677', is_active=True)
        # self.company1.save()
        self.company2 = Company.objects.create(city='יהוד', name='אביס', phone_num='888888888', is_active=True)
        # self.company2.save()
        self.company3 = Company.objects.create(city='ירושליים', name='אלבר', phone_num='777777777', is_active=False)
        # self.company3.save()
        self.car1 = Car.objects.create(name="פורד", timestamp = datetime.now())
        # self.car1.save()
        self.car2 = Car.objects.create(name="סובארו", timestamp = datetime.now())
        # self.car2.save()
        self.car3 = Car.objects.create(name="הונדה", timestamp = datetime.now())
        # self.car3.save()
        self.price1 = PriceList.objects.create(company=self.company1, car=self.car1, price_at_company=50)
        # self.price1.save()
        self.price2 = PriceList.objects.create(company=self.company2, car=self.car2, price_at_company=80)
        # self.price2.save()
        self.price3 = PriceList.objects.create(company=self.company3, car=self.car3, price_at_company=100)
        # self.price3.save()


    def test_min_price_for_company(self):
        c = Client()
        min_val_col_dic = {'min_val':[50,80,100]}
        min_val_df = pd.DataFrame(min_val_col_dic)
        response = c.get('/leasing/min-price-for-company/')
        df=pd.read_excel('min_price_for_company.xlsx')
        return self.assertItemsEqual(df['min_val'], min_val_df['min_val'])

    def test_active_contains_yud(self):
        c = Client()
        active_yud_phone_dic = {'phone_num':[888888888]}
        active_yud_phone_df = pd.DataFrame(active_yud_phone_dic)
        response = c.get('/leasing/active-contains-yud/')
        df=pd.read_excel('active_contain_yud.xlsx')
        return self.assertItemsEqual(df['phone_num'], active_yud_phone_df['phone_num'])

    def test_cars_from_active_companies(self):
        c = Client()
        cars_from_active_dic = {'cars': [u'פורד', u'סובארו']}
        cars_from_active_df = pd.DataFrame(cars_from_active_dic)
        response = c.get('/leasing/cars-from-active-companies/')
        df = pd.read_excel('cars_from_active.xlsx')
        return self.assertItemsEqual(cars_from_active_df['cars'], df['name'])









