# -*- coding: utf-8 -*-
from django.shortcuts import render
from django.http import HttpResponse
from .models import Car, Company, PriceList, CarQuerySet
from django_pandas.io import read_frame
from django.db.models import Min
from django.views.decorators.csrf import ensure_csrf_cookie



def index(request):
    template = 'index.html'
    return render(request, template)

@ensure_csrf_cookie
def present_query(request):
    if request.method == 'POST':
        query = request.POST.get('browser')
        if query == 'cars in active comp':
            query_result = Car.objects.cars_from_active_companies()
        if query == 'active contains yud':
            query_result = Company.objects.contains_yud().active_company()
        if query == 'min prices for comp':
            query_result= PriceList.objects.values('company').annotate(min_val= Min('price_at_company'))
        if query == 'price list by car':
            car = request.POST.get('car')
            query_result = PriceList.objects.price_list_by_car(car)
        if query == 'price list by company':
            company = request.POST.get('company')
            query_result = PriceList.objects.price_list_by_company(company)
        df = read_frame(query_result)
        data = df.to_html()
        page_context = {'data':data}
    return render(request, 'leasing/templates/index.html',page_context)


def all_cars_from_active_companies(request):
    cars_from_active = Car.objects.cars_from_active_companies()
    df = read_frame(cars_from_active)
    response = create_file_and_serve('cars_from_active.xlsx', df)
    return response

ZZ


def price_list_for_car(request, string):
    prices_for_car = PriceList.objects.price_list_by_car(string)
    df = read_frame(prices_for_car)
    response = create_file_and_serve('price_list_for_car.xlsx', df)
    return response


def price_list_for_company(request, string):
    prices_for_company = PriceList.objects.price_list_by_company(string).values('car__name', 'price_at_company')
    df = read_frame(prices_for_company)
    response = create_file_and_serve('price_list_for_company.xlsx', df)
    return response


def active_contains_yud(request):
    active_contains_yud = Company.objects.contains_yud().active_company()
    df = read_frame(active_contains_yud)
    response = create_file_and_serve('active_contain_yud.xlsx', df)
    return response


def min_price_for_company(request):
    min_price_for_company = PriceList.objects.values('company').annotate(min_val= Min('price_at_company'))
    df = read_frame(min_price_for_company)
    response = create_file_and_serve('min_price_for_company.xlsx', df)
    return response


def create_file_and_serve(file_name, df):
    df.to_excel(file_name, engine='openpyxl', index= False)
    with open(file_name, 'rb') as excel:
            data = excel.read()
    response = HttpResponse(data,content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
    response['Content-Disposition'] = 'attachment; filename={fl}'.format(fl=file_name)
    return response







