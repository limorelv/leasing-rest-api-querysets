from rest_framework.viewsets import ModelViewSet
from rest_framework_extensions.mixins import NestedViewSetMixin
from leasing.models import Car
from leasing.serializers.car_serializer import CarSerializer


class CarViewSet(NestedViewSetMixin, ModelViewSet):
    serializer_class = CarSerializer
    queryset = Car.objects.all()
