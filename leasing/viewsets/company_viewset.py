from rest_framework.viewsets import ModelViewSet
from rest_framework_extensions.mixins import NestedViewSetMixin
from leasing.models import Company
from leasing.serializers.company_serializer import CompanySerializer


class CompanyViewSet(NestedViewSetMixin, ModelViewSet):
    serializer_class = CompanySerializer
    queryset = Company.objects.all()
