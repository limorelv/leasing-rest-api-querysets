from rest_framework.viewsets import ModelViewSet
from rest_framework_extensions.mixins import NestedViewSetMixin
from leasing.models import PriceList
from leasing.serializers.pricelist_serializer import PriceListSerializer


class PriceListViewSet(NestedViewSetMixin, ModelViewSet):
    serializer_class = PriceListSerializer
    queryset = PriceList.objects.all()
