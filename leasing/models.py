# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from datetime import datetime
from django.utils.encoding import python_2_unicode_compatible
from django.db.models.query import QuerySet


# Create your models here.


class CompanyQuerySet(models.query.QuerySet):
    def active_company(self):
        return self.filter(is_active=True)

    def contains_yud(self):
        return self.filter(name__contains='י')



class CarQuerySet(models.query.QuerySet):
    def cars_from_active_companies(self):
        return self.filter(company__is_active= True).values('company', 'name')


class PriceListQuerySet(models.query.QuerySet):
    def price_list_by_company(self, string):
        return self.filter(company__name=string)


    def price_list_by_car(self, string):
        return self.filter(car__name=string)





@python_2_unicode_compatible
class Car(models.Model):
    name = models.CharField(max_length=200)
    timestamp = models.DateTimeField(default=datetime.now(), null=False)
    company = models.ManyToManyField('Company', through='PriceList')
    objects = CarQuerySet.as_manager()


    def __str__(self):
        return self.name




@python_2_unicode_compatible
class Company(models.Model):
    city_choices = ((u'חיפה', u'חיפה'),
                    (u'ירושליים', u'ירושליים'),
                    (u'יהוד',u'יהוד'))
    name = models.CharField(max_length=200)
    city = models.CharField(max_length=200, choices=city_choices)
    phone_num = models.CharField(max_length=20)
    is_active = models.BooleanField(default=False)
    objects = CompanyQuerySet.as_manager()

    def __str__(self):
        return self.name


@python_2_unicode_compatible
class PriceList (models.Model):
    company = models.ForeignKey('Company')
    car = models.ForeignKey('Car')
    price_at_company = models.FloatField()
    objects = PriceListQuerySet.as_manager()

    def __str__(self):
        return str(self.price_at_company)




