# -*- coding: utf-8 -*-
from . import views
from django.conf.urls import patterns, include, url
from django.contrib import admin
from rest_framework_extensions.routers import ExtendedDefaultRouter
from leasing.viewsets.car_viewset import CarViewSet
from leasing.viewsets.company_viewset import CompanyViewSet
from leasing.viewsets.PriceList_viewset import PriceListViewSet

# urlpatterns = [
#     url(r'^min-price-for-company/$', views.min_price_for_company, name= 'min_price_for_company'),
#     url(r'^price-list-for-car/(?P<string>.+)/$', views.price_list_for_car, name= 'price_list_for_car'),
#     url(r'^price-list-for-company/(?P<string>.+)/$', views.price_list_for_company, name= 'price_list_for_company'),
#     url(r'^$', views.index, name='index'),
#     url(r'^active_contains_yud/$', views.active_contains_yud, name= 'active_contains_yud')
#
# ]

admin.autodiscover()
router = ExtendedDefaultRouter()
(
    router.register('cars', CarViewSet, base_name= 'cars'),
    router.register('company', CompanyViewSet, base_name='company'),
    router.register('pricelist', PriceListViewSet, base_name='price_list')
    .register('dogs', CarViewSet, base_name='car', parents_query_lookups=['company'])
)

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^cars-from-active-companies/$', views.all_cars_from_active_companies, name= 'active_contains_yud'),
    url(r'^min-price-for-company/$', views.min_price_for_company, name= 'min_price_for_company'),
    url(r'^price-list-for-car/(?P<string>.+)/$', views.price_list_for_car, name= 'price_list_for_car'),
    url(r'^price-list-for-company/(?P<string>.+)/$', views.price_list_for_company, name= 'price_list_for_company'),
    url(r'^present-query/$', views.present_query, name='present-query'),
    url(r'^active-contains-yud/$', views.active_contains_yud, name= 'active_contains_yud'),
    url('', include(router.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),


]