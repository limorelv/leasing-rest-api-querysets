from rest_framework import serializers
from leasing.models import Car


class CarSerializer(serializers.ModelSerializer):
    class Meta:
        model = Car