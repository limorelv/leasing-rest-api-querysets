from rest_framework import serializers
from leasing.models import PriceList

class PriceListSerializer(serializers.ModelSerializer):
    class Meta:
        model = PriceList